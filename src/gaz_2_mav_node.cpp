/****************************************************************************

Conversion from vicon pose/quaternion to mavros pose/quaternion

Nodes:
subscribed pose and quat from Vicon (geometry_msgs::TransformStamped)
published  pose and quat to MAVROS (geometry_msgs::PoseStamped)
both in 

****************************************************************************/

#include <ros/ros.h>
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/Quaternion.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Pose.h>
#include <tf/transform_datatypes.h>
#include <tf/LinearMath/Matrix3x3.h>
#include <tf/LinearMath/Quaternion.h>
#include <gazebo_msgs/ModelStates.h>
#include <math.h>
#include <pthread.h>
#define PI 3.14159265

// p-thread calls
pthread_mutex_t topicBell;
pthread_cond_t newPose;

// Global Vars
int loop_hz = 100;
float max_val = 0.01;
int range_val = 4096;
int gaz_count = 0;
// Gazebo Pose
geometry_msgs::Pose gaz_pos;
geometry_msgs::PoseStamped gaz_pos_stamp;

float rand_gen(float max_val, int range)
{
	float r1 = (float)(rand() % range);
	r1 = r1-((float)range/2.0);
	r1 = (float)r1*max_val/range;
	return r1;
}
	

// Call back Function for Grabbing Vicon Info 
void gaz_Callback(const gazebo_msgs::ModelStates& msg)
{
  ++gaz_count;
  gaz_pos = msg.pose[1];
  pthread_cond_signal(&newPose);
}

int main(int argc, char **argv)
{ 
  //======= ROS Setup ================
  ros::init(argc, argv, "gaz_2_mav_node");
  ros::NodeHandle n; 
  //======= ROS Publishers ================
  ros::Publisher gaz_publisher = n.advertise<geometry_msgs::PoseStamped>("pose", 1);
  //======= ROS Subscribers ===============
  // Ros setup async spinners for subscibers
  ros::AsyncSpinner spinner(2);    
  // ROS setup subscriber
  ros::Subscriber gaz_subscriber = n.subscribe("gaz", 1, gaz_Callback);
  // Start the Spinner
  spinner.start();

   // Publisher Loop
  ros::Rate loop_rate(loop_hz);
  
  topicBell = PTHREAD_MUTEX_INITIALIZER;
  pthread_cond_init(&newPose, NULL);
  
  while (ros::ok())
  {
  	//ROS_INFO("gaz x=%f y=%f z=%f", gaz_pos.position.x, gaz_pos.position.y, gaz_pos.position.z);  
  	gaz_pos_stamp.header.seq = gaz_count;
  	gaz_pos_stamp.header.stamp = ros::Time::now();
  	gaz_pos_stamp.header.frame_id = "fcu";
  	
  	gaz_pos.position.x = gaz_pos.position.x + rand_gen(max_val,range_val);
  	gaz_pos.position.y = gaz_pos.position.y + rand_gen(max_val,range_val);
  	gaz_pos.position.z = gaz_pos.position.z + rand_gen(max_val,range_val);
  	  	 
  	gaz_pos_stamp.pose = gaz_pos;
    gaz_publisher.publish(gaz_pos_stamp);
    // Spin Once:
    ros::spinOnce();
	// Maintain loop rate
    loop_rate.sleep();
  }
  return 0;
}




